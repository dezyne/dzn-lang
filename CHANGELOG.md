# Change Log

All notable changes to the "dzn-lang" extension will be documented in this file.
## [v0.0.7]
- Added some more support for namespaces, specifically dotted names as port types
- Added dzn.async declaration support in behaviours

## [v0.0.6]
- Added rudimentary support for namespaces and import statements
  - 'namespace identifier {' works, but do not add newlines

## [v0.0.4]
- Added documentation and changelog

## [v0.0.3]
- Added syntax highlighting for guards

## [v0.0.2]
- Added syntax highlighting for variable declarations

## [v0.0.1]
- Initial release, includes syntax highlighting for:
    - Models (interface/component)
    - Event declarations
    - Port declarations
    - Enum declarations
    - Behaviours
    - Event triggers and actions
    - Replies
- Code folding
- Inline and multi-line comments
