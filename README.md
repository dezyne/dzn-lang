# dzn-lang: Syntax support for Dezyne
This extension adds basic syntax support for the [Dezyne](https://www.dezyne.org) modeling language. For more involved semantic language support, we refer to our **upcoming** Dezyne LSP implementation.

## Features
Syntax highlighting currently works best using [Community Material Theme](https://marketplace.visualstudio.com/items?itemName=Equinusocio.vsc-material-theme). Syntax highlighting is based on Textmate and generated using [Iro](https://eeyo.io/iro/documentation/):

Before  |   After
:------:|:-------:
<img src="doc/before.PNG" alt="Before" style="width:65%;"> | <img src="doc/after.PNG" alt="After" style="width:65%;">

Code folding on matching brackets:

<img src="doc/code_folding.gif" style="width:35%">

Comment line / comment region:

<img src="doc/comments.gif" style="width:35%">

## Contact us

Please email [Support](mailto:support@verum.com) for help or questions; email bug
reports to [Bugs: Editor](mailto:bugs-editor@verum.com) or visit [Issues](https://gitlab.com/dezyne/dzn-lang/-/issues).

Join #dezyne on irc.freenode.net.

## License
This software is planned to be released under the GPLV3+ license; to be added.