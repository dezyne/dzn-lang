import json, sys
from plistlib import load

with open(sys.path[0] + "/tm.plist", 'rb') as fp:
    foo = {"$schema": "https://raw.githubusercontent.com/martinring/tmlanguage/master/tmlanguage.json"}
    foo.update(load(fp))

with open(sys.path[0] + "/tm.json", 'w') as fp:
    json.dump(foo, fp, indent=4)
